#include "Menu.h"

using namespace std;

Menu::Menu(map<string, Customer> abcCustomers, Item itemList[], int size)
{
	_abcCustomers = abcCustomers;
	_itemList = itemList;
	_size = size;
}

int Menu::getNumber()
{
	cout << "\033[0;30m";
	int num = 0;
	cin >> num;
	cout << "\033[0m";
	return num;
}

int Menu::getNumber(int min, int max)
{
	int num = 0;
	
	do
	{
		num = getNumber();
	} while (num < min || num > max);

	return num;
}

void Menu::startMain()
{
	while (true)
	{
		system("cls");

		cout << "\033[33mWelcome to MagshiMart!" << endl;
		cout << "\033[0;1m1. \033[0mto sign as customer and buy items" << endl;
		cout << "\033[0;1m2. \033[0mto update existing customer's items" << endl;
		cout << "\033[0;1m3. \033[0mto print the customer who pays the most" << endl;
		cout << "\033[0;1m4. \033[0mto exit" << endl;

		switch (getNumber(1, 4))
		{
		case 1:
			startItemsAdder(startSignUp());
			break;
		case 2:
			startEditMenu(startLogIn());
			break;
		case 3:
			startBestCustomer();
			break;
		case 4:
			return;
		}
	}
}

void Menu::startBestCustomer()
{
	system("cls");

	if (_abcCustomers.size() == 0)
	{
		cout << "\033[31mThere aren't any customers." << endl;
	}
	else
	{
		Customer* customer = nullptr;
		
		int amount = 0;

		map<string, Customer>::iterator it;
		for (it = _abcCustomers.begin(); it != _abcCustomers.end(); it++)
		{
			if (it->second.totalSum() > amount)
			{
				customer = &it->second;
				amount = customer->totalSum();
			}
		}

		if (customer)
		{
			cout << "\033[0;32;4mName: \033[0;1m" << customer->getName() << endl;
			cout << "\033[0;32;4mSummary: \033[0;1m" << customer->totalSum() << endl;
			printItems(customer);
		}
	}
	
	cout << endl << "\033[0;35;1;4m";
	system("pause");
	cout << "\033[0m";
}


Customer* Menu::startSignUp()
{
	system("cls");

	string name = "";

	cout << "\033[32;4mEnter your name:\033[0;1m ";
	cin >> name;

	if (_abcCustomers.find(name) != _abcCustomers.end())
		return nullptr;
	
	Customer* customer = new Customer(name);

	_abcCustomers.insert(pair<string, Customer>(name, *customer));

	return &_abcCustomers.at(name);
}

Customer* Menu::startLogIn()
{
	system("cls");

	string name = "";

	cout << "\033[32;4mEnter your name:\033[0;1m ";
	cin >> name;

	if (_abcCustomers.find(name) == _abcCustomers.end())
		return nullptr;

	return &_abcCustomers.at(name);
}

void Menu::printItems()
{
	cout << "\033[0;32;4mThe items you can buy are:\033[0m" << endl;
	for (int i = 0; i < _size; i++)
	{
		Item item = _itemList[i];
		cout << "\033[0;1m" << i + 1 << ". " << item.getName() << "\033[0m - \033[0;32;4;1mprice:\033[0;1m " << item.getUnitPrice() << endl;
	}
}

vector<Item> Menu::printItems(Customer* customer)
{
	vector<Item> items = *new vector<Item>();
	for (Item item : customer->getItems())
		items.push_back(item);

	cout << "\033[0;32;4mItems:\033[0m";
	for (int i = 0; i < items.size(); i++)
	{
		Item item = items[i];
		cout << endl << "\033[0;1m" << i + 1 << ". " << item.getName() << endl << "   \033[0;32;4;1mcount:\033[0;1m x" << item.getCount() << endl << "   \033[0;32;4mtotal price:\033[0;1m " << item.totalPrice();
	}
	cout << "\033[0m" << endl << endl;

	return items;
}


void Menu::startEditMenu(Customer* customer)
{
	if (!customer)
		startMain();

	system("cls");
	
	printItems(customer);

	cout << "\033[0;1m1. \033[0madd items" << endl;
	cout << "\033[0;1m2. \033[0mremove items" << endl;
	cout << "\033[0;1m3. \033[0mback to menu" << endl;

	switch (getNumber(1, 3))
	{
	case 1:
		startItemsAdder(customer);
		break;
	case 2:
		startItemsRemover(customer);
		break;
	case 3:
		return;
	}

	startEditMenu(customer);
}

void Menu::startItemsAdder(Customer* customer)
{
	if (!customer)
		return;

	system("cls");

	printItems();
	cout << endl << "\033[0mWhat item would you like to buy? (Enter 0 to cancel)" << endl << "\033[32;4mInput:\033[0;1m ";

	const int input = getNumber(0, _size);

	if (!input)
		return;

	customer->addItem(_itemList[input - 1]);

	startItemsAdder(customer);
}

void Menu::startItemsRemover(Customer* customer)
{
	if (!customer)
		return;

	system("cls");

	vector<Item> items = printItems(customer);
	
	if (items.size() == 0)
		return;

	cout << "\033[0mWhat item would you like to remove? (Enter 0 to cancel)" << endl << "\033[32;4mInput:\033[0;1m ";

	const int input = getNumber(0, _size);

	if (!input)
		return;

	customer->removeItem(items[input - 1]);

	startItemsRemover(customer);
}
