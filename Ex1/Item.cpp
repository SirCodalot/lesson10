#include "Item.h"

Item::Item(string name, string serialNumber, double unitPrice)
{
	_name = name;
	_serialNumber = serialNumber;
	_count = 1;
	_unitPrice = unitPrice;
}

Item::~Item()
{
	// TODO
}

string Item::getName() const
{
	return _name;
}

string Item::getSerialNumber() const
{
	return _serialNumber;
}

int Item::getCount() const
{
	return _count;
}

double Item::getUnitPrice() const
{
	return _unitPrice;
}

double Item::totalPrice() const
{
	return _count * _unitPrice;
}

void Item::setName(string name)
{
	_name = name;
}

void Item::setSerialNumber(string serialNumber)
{
	_serialNumber = serialNumber;
}

void Item::setCount(int count)
{
	_count = count;
}

void Item::setUnitPrice(double unitPrice)
{
	_unitPrice = unitPrice;
}

bool Item::operator<(const Item& other) const
{
	return _serialNumber < other.getSerialNumber();
}

bool Item::operator>(const Item& other) const
{
	return _serialNumber > other.getSerialNumber();
}

bool Item::operator==(const Item& other) const
{
	return _serialNumber == other.getSerialNumber();
}
