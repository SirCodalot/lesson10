#pragma once

#include "Customer.h"
#include <map>
#include <vector>

class Menu
{
public:
	Menu(map<string, Customer> abcCustomers, Item itemList[], int size);

	void startMain();
	void startBestCustomer();

	void startEditMenu(Customer* customer);

	void startItemsAdder(Customer* customer);
	void startItemsRemover(Customer* customer);
	
private:
	void printItems();
	vector<Item> printItems(Customer* customer);

	Customer* startSignUp();
	Customer* startLogIn();
	
	int getNumber();
	int getNumber(int min, int max);

	map<string, Customer> _abcCustomers;
	Item* _itemList;
	int _size;
};