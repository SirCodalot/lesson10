#include "Customer.h"

 Customer::Customer(string name)
 {
	 _name = name;
	 _items = *new set<Item>();
 }

 Customer::~Customer()
 {
	 
 }

string Customer::getName() const
{
	return _name;
}

set<Item> Customer::getItems() const
{
	return _items;
}

void Customer::setName(string name)
{
	_name = name;
}

void Customer::addItem(Item item)
{
	set<Item>::iterator it;
	int count = 0;

 	for (it = _items.begin(); it != _items.end(); it++)
	{
		if (item.getSerialNumber() == it->getSerialNumber())
		{
			count = it->getCount() + item.getCount();
			break;
		}
	}

	Item newItem = *new Item(item);

	if (count)
	{
		_items.erase(it);
		newItem.setCount(count);
	}

	_items.insert(newItem);
}

void Customer::removeItem(Item item)
{
	set<Item>::iterator it;
	int count = 0;
	bool found = false;
	for (it = _items.begin(); it != _items.end(); it++)
	{
		if (item.getSerialNumber() == it->getSerialNumber())
		{
			count = it->getCount() - 1;
			found = true;
			break;
		}
	}

	if (found)
	{
		_items.erase(it);
		if (count > 0)
		{
			Item newItem = *new Item(item);
			newItem.setCount(count);
			_items.insert(newItem);
		}
	}
}

double Customer::totalSum() const
{
	double sum = 0;

	for (Item item : _items)
	{
		sum += item.totalPrice();
	}

	return sum;
}
