#pragma once

#include<iostream>
#include<string>
#include<algorithm>

using namespace std;

class Item
{
public:
	Item(string name, string serialNumber, double unitPrice);
	~Item();

	double totalPrice() const; //returns _count*_unitPrice
	
	bool operator <(const Item& other) const; //compares the _serialNumber of those items.
	bool operator >(const Item& other) const; //compares the _serialNumber of those items.
	bool operator ==(const Item& other) const; //compares the _serialNumber of those items.

											   //get and set functions
	string getName() const;
	string getSerialNumber() const;
	int getCount() const;
	double getUnitPrice() const;

	void setName(string name);
	void setSerialNumber(string serialNumber);
	void setCount(int count);
	void setUnitPrice(double unitPrice);

private:
	string _name;
	string _serialNumber; //consists of 5 numbers
	int _count; //default is 1, can never be less than 1!
	double _unitPrice; //always bigger than 0!

};